-- -----------------------------------------------------
-- Data for table `PetAssist`.`Disease`
-- -----------------------------------------------------
START TRANSACTION;
USE `PetAssist`;
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`) VALUES ('Bordetella Bronchiseptica', 'Kennel Cough');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Canine Distemper', NULL,                     'coughing', 'runny nose', 'lethargy', 'anorexia', 'diarrhea', 'watery eyes');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Canine Hepatitis', NULL,                     'runny nose', 'lethargy', 'loss of appetite', 'runny eyes', 'vomiting', 'blueish eyes');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Canine Parainfluenza', NULL, 'Canine Cough', 'coughing', 'runny nose', 'lethargy', 'sneezing', 'watery eyes');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Corona Virus', NULL,                         'coughing', 'diarrhea', 'nausea', 'lack of appetite', 'vomiting', 'hard/bloated belly');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Heartworm', NULL,                            'coughing', 'reluctance to exercise', 'fatigue', 'loss of appetite', 'weight loss', 'swollen belly');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Tracheobronchitis', 'Kennel Cough',          'coughing', 'runny nose', 'lethargy', 'sneezing', 'loss of appetite', 'low fever');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Leptospirosis', NULL,                        'sudden fever', 'sore muscles', 'stiffness', 'shivering', 'weakness', 'lack of appetite');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Lyme Disease', NULL,                         'fever', 'loss of appetite', 'reduced energy', 'lameness', 'stiffness', 'swollen joints');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Parovivirus', 'Parvo',                       'fever', 'lethargy', 'bloody diarrhea', 'loss of appetite', 'malaise', 'vomiting');
INSERT INTO `PetAssist`.`Disease` (`scientific_name`, `common_name`, `symptom_1`, `symptom_2`, `symptom_3`, `symptom_4`, `symptom_5`, `symptom_6`) VALUES ('Rabies', NULL,                               'fever', 'personality change', 'repeated isolated licking', 'photosensitivity', 'paralysis of the throat/jaw', 'loss of appetite');

COMMIT;


-- -----------------------------------------------------
-- Data for table `PetAssist`.`Breed`
-- -----------------------------------------------------
START TRANSACTION;
USE `PetAssist`;
INSERT INTO `PetAssist`.`Breed` (`breed_id`, `breed_name`) VALUES (DEFAULT, 'Husky');
INSERT INTO `PetAssist`.`Breed` (`breed_id`, `breed_name`) VALUES (DEFAULT, 'Morkie');
INSERT INTO `PetAssist`.`Breed` (`breed_id`, `breed_name`) VALUES (DEFAULT, 'Pitbull');
INSERT INTO `PetAssist`.`Breed` (`breed_id`, `breed_name`) VALUES (DEFAULT, 'Yorkie');

COMMIT;
